const isDev = process.env.NODE_ENV === 'development';

export const INTERNAL_BASE_URL = 'http://localhost:3000';
