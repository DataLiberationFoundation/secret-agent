export default interface IConfigureOptions {
  maxActiveSessionCount?: number;
  localProxyPortStart?: number;
  sessionsDir?: string;
}
