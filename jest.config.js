module.exports = {
  preset: 'ts-jest',
  verbose: false,
  setupFilesAfterEnv: ['./jest-each.js'],
  testMatch: ['**/test/*.test.ts'],
  testEnvironment: './jest-env.js',
  collectCoverage: false,
  collectCoverageFrom: [
    'client/**/*.{ts}',
    'core/**/*.{ts}',
    'core-server/**/*.{ts}',
    'emulators/**/*.{ts}',
    'full-client/**/*.{ts}',
    'humanoids/**/*.{ts}',
    'mitm/**/*.{ts}',
    'remote-client/**/*.{ts}',
    'replay-api/**/*.{ts}',
    'shared/**/*.{ts}',
  ],
  coveragePathIgnorePatterns: ['index.js'],
  modulePathIgnorePatterns: ['jest-env.js', 'dist', 'config'],
  reporters: [
    'default',
    [
      'jest-summary-reporter',
      {
        failuresOnly: true,
      },
    ],
  ],
  roots: [
    'client/',
    'core/',
    'core-server/',
    'emulators/',
    'emulator-plugins/',
    'full-client/',
    'humanoids/',
    'humanoid-plugins/',
    'mitm/',
    'remote-client/',
    'replay-api/',
    'shared/',
  ],
  moduleDirectories: [
    'node_modules',
    'client/node_modules',
    'core/node_modules',
    'core-server/node_modules',
    'emulators/node_modules',
    'full-client/node_modules',
    'humanoids/node_modules',
    'mitm/node_modules',
    'remote-client/node_modules',
    'replay-api/**/node_modules',
    'shared/**/node_modules',
  ],
};
