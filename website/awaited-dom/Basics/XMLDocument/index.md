# XMLDocument

<div class='overview'>The <strong>XMLDocument</strong> interface represents an XML document. It inherits from the generic <a href="/en-US/docs/Web/API/Document" title="The Document interface represents any web page loaded in the browser and serves as an entry point into the web page's content, which is the DOM tree."><code>Document</code></a> and does not add any specific methods or properties to it: nevertheless, several algorithms behave differently with the two types of documents.</div>

## Properties

<ul class="items properties">

</ul>

## Methods

<ul class="items methods">

</ul>

## Events
