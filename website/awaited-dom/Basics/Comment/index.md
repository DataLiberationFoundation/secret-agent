# Comment

<div class='overview'><span class="seoSummary">The <code><strong>Comment</strong></code> interface represents textual notations within markup; although it is generally not visually shown, such comments are available to be read in the source view.</span> Comments are represented in HTML and XML as content between '<code>&lt;!--</code>' and '<code>--&gt;</code>'. In XML, the character sequence '<code>--</code>' cannot be used within a comment.</div>

## Properties

<ul class="items properties">

</ul>

## Methods

<ul class="items methods">

</ul>

## Events
