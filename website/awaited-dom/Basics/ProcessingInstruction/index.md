# ProcessingInstruction

<div class='overview'><span class="seoSummary">The <code><strong>ProcessingInstruction</strong></code> interface represents a <a class="external" href="https://www.w3.org/TR/xml/#sec-pi" rel="noopener">processing instruction</a>; that is, a <a href="/en-US/docs/Web/API/Node" title="Node is an interface from which various types of DOM API objects inherit, allowing those types to be treated similarly; for example, inheriting the same set of methods, or being testable in the same way."><code>Node</code></a> which embeds an instruction targeting a specific application but that can be ignored by any other applications which don't recognize the instruction.</span></div>

<div class='overview'>A processing instruction is different from the <a href="/en-US/docs/Web/XML/XML_introduction#XML_declaration">XML declaration</a>.</div>

## Properties

<ul class="items properties">
  <li>
    <a href="">target</a>
    <div>A name identifying the application to which the instruction is targeted,</div>
  </li>
</ul>

## Methods

<ul class="items methods">

</ul>

## Events
