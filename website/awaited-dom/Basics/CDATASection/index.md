# CDATASection

<div class='overview'>The <code><strong>CDATASection</strong></code> interface represents a CDATA section that can be used within XML to include extended portions of unescaped text. The symbols <code>&lt;</code> and <code>&amp;</code> don’t need escaping as they normally do when inside a CDATA section.</div>

<div class='overview'>In XML, a CDATA section looks like:</div>

## Properties

<ul class="items properties">

</ul>

## Methods

<ul class="items methods">

</ul>

## Events
