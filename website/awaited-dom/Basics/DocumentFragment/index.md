# DocumentFragment

<div class='overview'><span class="seoSummary">The <strong><code>DocumentFragment</code></strong> interface represents a minimal document object that has no parent. It is used as a lightweight version of <a href="/en-US/docs/Web/API/Document" title="The Document interface represents any web page loaded in the browser and serves as an entry point into the web page's content, which is the DOM tree."><code>Document</code></a> that stores a segment of a document structure comprised of nodes just like a standard document.</span> The key difference is due to the fact that&nbsp;the&nbsp;document fragment isn't part of the active document tree structure.&nbsp;Changes made to the fragment don't affect the document (even on&nbsp;<a class="glossaryLink" href="/en-US/docs/Glossary/reflow" title="reflow: Reflow&nbsp;happens when a browser must process and draw part or all of a webpage again, such as after an update on an interactive site.">reflow</a>)&nbsp;or incur any performance impact when changes are made.</div>

## Properties

<ul class="items properties">

</ul>

## Methods

<ul class="items methods">

</ul>

## Events
