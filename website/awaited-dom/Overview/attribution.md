# Attribution

Most of the DOM documentation contained on this website ultimately comes from the W3C specification files. A huge thanks to the Mozilla organization for compiling all this data into a single location.
