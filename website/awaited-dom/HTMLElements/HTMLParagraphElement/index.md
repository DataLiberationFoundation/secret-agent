# HTMLParagraphElement

<div class='overview'>The <strong><code>HTMLParagraphElement</code></strong> interface provides special properties (beyond those of the regular <a href="/en-US/docs/Web/API/HTMLElement" title="The HTMLElement interface represents any HTML element. Some elements directly implement this interface, while others implement it via an interface that inherits it."><code>HTMLElement</code></a> object interface it inherits) for manipulating <a href="/en-US/docs/Web/HTML/Element/p" title="The HTML <p> element represents a paragraph."><code>&lt;p&gt;</code></a> elements.</div>

## Properties

<ul class="items properties">
  <li>
    <a href="">align</a>
    <div>A <a href="/en-US/docs/Web/API/DOMString" title="DOMString is a UTF-16 String. As JavaScript already uses such strings, DOMString is mapped directly to a String."><code>DOMString</code></a> representing an enumerated property indicating alignment of the element's contents with respect to the surrounding context. The possible values are <code>"left"</code>, <code>"right"</code>, <code>"justify"</code>, and <code>"center"</code>.</div>
  </li>
</ul>

## Methods

<ul class="items methods">

</ul>

## Events
