# HTMLHtmlElement

<div class='overview'>The <strong><code>HTMLHtmlElement</code></strong> interface serves as the root node for a given HTML document. This object inherits the properties and methods described in the <a href="/en-US/docs/Web/API/HTMLElement" title="The HTMLElement interface represents any HTML element. Some elements directly implement this interface, while others implement it via an interface that inherits it."><code>HTMLElement</code></a> interface.</div>

<div class='overview'>You can retrieve the <code>HTMLHtmlElement</code> object for a given document by reading the value of the <a href="/en-US/docs/Web/API/Document/documentElement" title="Document.documentElement returns the Element that is the root element of the document (for example, the <html> element for HTML documents)."><code>document.documentElement</code></a> property.</div>

## Properties

<ul class="items properties">
  <li>
    <a href="">version</a>
    <div>Is a <a href="/en-US/docs/Web/API/DOMString" title="DOMString is a UTF-16 String. As JavaScript already uses such strings, DOMString is mapped directly to a String."><code>DOMString</code></a> representing the version of the HTML Document Type Definition (DTD) that governs this document. This property should not be used any more as it is non-conforming. Simply omit it.</div>
  </li>
</ul>

## Methods

<ul class="items methods">

</ul>

## Events
