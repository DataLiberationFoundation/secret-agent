# HTMLTableColElement

<div class='overview'>The <strong><code>HTMLTableColElement</code></strong> interface provides special properties (beyond the <a href="/en-US/docs/Web/API/HTMLElement" title="The HTMLElement interface represents any HTML element. Some elements directly implement this interface, while others implement it via an interface that inherits it."><code>HTMLElement</code></a> interface it also has available to it inheritance) for manipulating single or grouped table column elements.</div>

## Properties

<ul class="items properties">
  <li>
    <a href="">align</a>
    <div>Indicates the horizontal alignment of the cell data in the column.</div>
  </li>
  <li>
    <a href="">ch</a>
    <div>Alignment character for cell data.</div>
  </li>
  <li>
    <a href="">chOff</a>
    <div>Offset for the alignment character.</div>
  </li>
  <li>
    <a href="">span</a>
    <div>Reflects the <code><a href="/en-US/docs/Web/HTML/Element/col#attr-span">span</a></code> HTML&nbsp;attribute, indicating the number of columns to apply this object's attributes to. Must be a positive integer.</div>
  </li>
  <li>
    <a href="">vAlign</a>
    <div>Indicates the vertical alignment of the cell data in the column.</div>
  </li>
  <li>
    <a href="">width</a>
    <div>Default column width.</div>
  </li>
</ul>

## Methods

<ul class="items methods">

</ul>

## Events
