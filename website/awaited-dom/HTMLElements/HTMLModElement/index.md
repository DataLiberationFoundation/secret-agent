# HTMLModElement

<div class='overview'>The <strong><code>HTMLModElement</code></strong> interface provides special properties (beyond the regular methods and properties available through the <a href="/en-US/docs/Web/API/HTMLElement" title="The HTMLElement interface represents any HTML element. Some elements directly implement this interface, while others implement it via an interface that inherits it."><code>HTMLElement</code></a> interface they also have available to them by inheritance) for manipulating modification elements, that is <a href="/en-US/docs/Web/HTML/Element/del" title="The HTML <del> element represents a range of text that has been deleted from a document."><code>&lt;del&gt;</code></a> and <a href="/en-US/docs/Web/HTML/Element/ins" title="The HTML <ins> element represents a range of text that has been added to a document."><code>&lt;ins&gt;</code></a>.</div>

## Properties

<ul class="items properties">
  <li>
    <a href="">cite</a>
    <div>Is a <a href="/en-US/docs/Web/API/DOMString" title="DOMString is a UTF-16 String. As JavaScript already uses such strings, DOMString is mapped directly to a String."><code>DOMString</code></a> reflecting the <code><a href="/en-US/docs/Web/HTML/Element/del#attr-cite">cite</a></code> HTML attribute, containing a URI of a resource explaining the change.</div>
  </li>
  <li>
    <a href="">dateTime</a>
    <div>Is a <a href="/en-US/docs/Web/API/DOMString" title="DOMString is a UTF-16 String. As JavaScript already uses such strings, DOMString is mapped directly to a String."><code>DOMString</code></a> reflecting the <code><a href="/en-US/docs/Web/HTML/Element/del#attr-datetime">datetime</a></code> HTML attribute, containing a date-and-time string representing a timestamp for the change.</div>
  </li>
</ul>

## Methods

<ul class="items methods">

</ul>

## Events
