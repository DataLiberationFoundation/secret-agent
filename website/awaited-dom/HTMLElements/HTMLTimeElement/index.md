# HTMLTimeElement

<div class='overview'>The <strong><code>HTMLTimeElement</code></strong> interface provides special properties (beyond the regular <a href="/en-US/docs/Web/API/HTMLElement" title="The HTMLElement interface represents any HTML element. Some elements directly implement this interface, while others implement it via an interface that inherits it."><code>HTMLElement</code></a> interface it also has available to it by inheritance) for manipulating <a href="/en-US/docs/Web/HTML/Element/time" title="The HTML <time> element represents a specific period in time."><code>&lt;time&gt;</code></a> elements.</div>

## Properties

<ul class="items properties">
  <li>
    <a href="">dateTime</a>
    <div>Is a <a href="/en-US/docs/Web/API/DOMString" title="DOMString is a UTF-16 String. As JavaScript already uses such strings, DOMString is mapped directly to a String."><code>DOMString</code></a> that reflects the <code><a href="/en-US/docs/Web/HTML/Element/time#attr-datetime">datetime</a></code> HTML attribute, containing a machine-readable form of the element's date and time value.</div>
  </li>
</ul>

## Methods

<ul class="items methods">

</ul>

## Events
