# HTMLDataListElement

<div class='overview'>The <strong><code>HTMLDataListElement</code></strong> interface provides special properties (beyond the <a href="/en-US/docs/Web/API/HTMLElement" title="The HTMLElement interface represents any HTML element. Some elements directly implement this interface, while others implement it via an interface that inherits it."><code>HTMLElement</code></a> object interface it also has available to it by inheritance) to manipulate <a href="/en-US/docs/Web/HTML/Element/datalist" title="The HTML <datalist> element contains a set of <option> elements that represent the permissible or recommended options available to choose from within other controls."><code>&lt;datalist&gt;</code></a> elements and their content.</div>

## Properties

<ul class="items properties">
  <li>
    <a href="">options</a>
    <div>Is a <a href="/en-US/docs/Web/API/HTMLCollection" title="The HTMLCollection interface represents a generic collection (array-like object similar to arguments) of elements (in document order) and offers methods and properties for selecting from the list."><code>HTMLCollection</code></a> representing a collection of the contained option elements.</div>
  </li>
</ul>

## Methods

<ul class="items methods">

</ul>

## Events
