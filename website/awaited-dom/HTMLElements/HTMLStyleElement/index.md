# HTMLStyleElement

<div class='overview'>The <strong><code>HTMLStyleElement</code></strong> interface represents a <a href="/en-US/docs/Web/HTML/Element/style" title="The HTML <style> element contains style information for a document, or part of a document."><code>&lt;style&gt;</code></a> element. It inherits properties and methods from its parent, <a href="/en-US/docs/Web/API/HTMLElement" title="The HTMLElement interface represents any HTML element. Some elements directly implement this interface, while others implement it via an interface that inherits it."><code>HTMLElement</code></a>, and from <a href="/en-US/docs/Web/API/LinkStyle" title="The LinkStyle interface provides access to the associated CSS style sheet of a node."><code>LinkStyle</code></a>.</div>

<div class='overview'>This interface doesn't allow to manipulate the CSS it contains (in most case). To manipulate CSS, see <a href="/en-US/docs/Web/API/CSS_Object_Model/Using_dynamic_styling_information">Using dynamic styling information</a> for an overview of the objects used to manipulate specified CSS properties using the DOM.</div>

## Properties

<ul class="items properties">
  <li>
    <a href="">media</a>
    <div>Is a <a href="/en-US/docs/Web/API/DOMString" title="DOMString is a UTF-16 String. As JavaScript already uses such strings, DOMString is mapped directly to a String."><code>DOMString</code></a> representing the intended destination medium for style information.</div>
  </li>
  <li>
    <a href="">type</a>
    <div>Is a <a href="/en-US/docs/Web/API/DOMString" title="DOMString is a UTF-16 String. As JavaScript already uses such strings, DOMString is mapped directly to a String."><code>DOMString</code></a> representing the type of style being applied by this statement.</div>
  </li>
</ul>

## Methods

<ul class="items methods">

</ul>

## Events
