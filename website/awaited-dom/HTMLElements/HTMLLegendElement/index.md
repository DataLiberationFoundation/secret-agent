# HTMLLegendElement

<div class='overview'>The <strong><code>HTMLLegendElement</code></strong> is an interface allowing to access properties of the <a href="/en-US/docs/Web/HTML/Element/legend" title="The HTML <legend> element represents a caption for the content of its parent <fieldset>."><code>&lt;legend&gt;</code></a> elements. It inherits properties and methods from the <a href="/en-US/docs/Web/API/HTMLElement" title="The HTMLElement interface represents any HTML element. Some elements directly implement this interface, while others implement it via an interface that inherits it."><code>HTMLElement</code></a> interface.</div>

## Properties

<ul class="items properties">
  <li>
    <a href="">align</a>
    <div>Alignment relative to the form set</div>
  </li>
  <li>
    <a href="">form</a>
    <div><p>The form that this legend belongs to. If the legend has a fieldset element as its parent, then this attribute returns the same value as the <strong>form</strong> attribute on the parent fieldset element. Otherwise, it returns null.</p></div>
  </li>
</ul>

## Methods

<ul class="items methods">

</ul>

## Events
