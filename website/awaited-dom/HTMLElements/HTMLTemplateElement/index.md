# HTMLTemplateElement

<div class='overview'>The <code><strong>HTMLTemplateElement</strong></code> interface enables access to the contents of an HTML <a href="/en-US/docs/Web/HTML/Element/template" title="The HTML Content Template (<template>) element is a mechanism for holding HTML that is not to be rendered immediately when a page is loaded but may be instantiated subsequently during runtime using JavaScript."><code>&lt;template&gt;</code></a> element.</div>

## Properties

<ul class="items properties">
  <li>
    <a href="">content</a>
    <div>A read-only <a href="/en-US/docs/Web/API/DocumentFragment" title="The DocumentFragment interface represents a minimal document object that has no parent. It is used as a lightweight version of Document that stores a segment of a document structure comprised of nodes just like a standard document."><code>DocumentFragment</code></a> which contains the DOM subtree representing the <a href="/en-US/docs/Web/HTML/Element/template" title="The HTML Content Template (<template>) element is a mechanism for holding HTML that is not to be rendered immediately when a page is loaded but may be instantiated subsequently during runtime using JavaScript."><code>&lt;template&gt;</code></a> element's template contents.</div>
  </li>
</ul>

## Methods

<ul class="items methods">

</ul>

## Events
