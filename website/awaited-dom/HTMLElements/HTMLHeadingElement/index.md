# HTMLHeadingElement

<div class='overview'>The <strong><code>HTMLHeadingElement</code></strong> interface represents the different heading elements. It inherits methods and properties from the <a href="/en-US/docs/Web/API/HTMLElement" title="The HTMLElement interface represents any HTML element. Some elements directly implement this interface, while others implement it via an interface that inherits it."><code>HTMLElement</code></a> interface.</div>

## Properties

<ul class="items properties">
  <li>
    <a href="">align</a>
    <div>Is a <a href="/en-US/docs/Web/API/DOMString" title="DOMString is a UTF-16 String. As JavaScript already uses such strings, DOMString is mapped directly to a String."><code>DOMString</code></a> representing an enumerated attribute indicating alignment of the heading with respect to the surrounding context.&nbsp;The possible values are&nbsp;<code>"left"</code>,&nbsp;<code>"right"</code>,&nbsp;<code>"justify"</code>, and&nbsp;<code>"center"</code>.</div>
  </li>
</ul>

## Methods

<ul class="items methods">

</ul>

## Events
