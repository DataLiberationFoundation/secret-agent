# HTMLUnknownElement

<div class='overview'>The <strong><code>HTMLUnknownElement</code></strong> interface represents an invalid HTML element and derives from the <a href="/en-US/docs/Web/API/HTMLElement" title="The HTMLElement interface represents any HTML element. Some elements directly implement this interface, while others implement it via an interface that inherits it."><code>HTMLElement</code></a> interface, but without implementing any additional properties or methods.</div>

## Properties

<ul class="items properties">

</ul>

## Methods

<ul class="items methods">

</ul>

## Events
