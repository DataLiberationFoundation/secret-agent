# HTMLFontElement

<div class='overview'><strong>Obsolete</strong><br>This feature is obsolete. Although it may still work in some browsers, its use is discouraged since it could be removed at any time. Try to avoid using it.</div>

## Properties

<ul class="items properties">
  <li>
    <a href="">color</a>
    <div>Is a <a href="/en-US/docs/Web/API/DOMString" title="DOMString is a UTF-16 String. As JavaScript already uses such strings, DOMString is mapped directly to a String."><code>DOMString</code></a> that reflects the <code><a href="/en-US/docs/Web/HTML/Element/font#attr-color">color</a></code> HTML attribute, containing either a named color or a color specified in the hexadecimal #RRGGBB format.</div>
  </li>
  <li>
    <a href="">face</a>
    <div>Is a <a href="/en-US/docs/Web/API/DOMString" title="DOMString is a UTF-16 String. As JavaScript already uses such strings, DOMString is mapped directly to a String."><code>DOMString</code></a> that reflects the <code><a href="/en-US/docs/Web/HTML/Element/font#attr-face">face</a></code> HTML attribute, containing a comma-separated list of one or more font names.</div>
  </li>
  <li>
    <a href="">size</a>
    <div>Is a <a href="/en-US/docs/Web/API/DOMString" title="DOMString is a UTF-16 String. As JavaScript already uses such strings, DOMString is mapped directly to a String."><code>DOMString</code></a> that reflects the <code><a href="/en-US/docs/Web/HTML/Element/font#attr-size">size</a></code> HTML attribute, containing either a font size number ranging from 1 to 7 or a relative size to the <code><a href="/en-US/docs/Web/HTML/Element/basefont#attr-size">size</a></code> attribute of the <a href="/en-US/docs/Web/HTML/Element/basefont" title="The obsolete HTML Base Font element (<basefont>) sets a default font face, size, and color for the other elements which are descended from its parent element."><code>&lt;basefont&gt;</code></a> element, for example -2 or +1.</div>
  </li>
</ul>

## Methods

<ul class="items methods">

</ul>

## Events
