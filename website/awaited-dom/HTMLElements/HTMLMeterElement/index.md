# HTMLMeterElement

<div class='overview'>The HTML <a href="/en-US/docs/Web/HTML/Element/meter" title="The HTML <meter> element represents either a scalar value within a known range or a fractional value."><code>&lt;meter&gt;</code></a> elements expose the <code><strong>HTMLMeterElement</strong></code> interface, which provides special properties and methods (beyond the <a href="/en-US/docs/Web/API/HTMLElement" title="The HTMLElement interface represents any HTML element. Some elements directly implement this interface, while others implement it via an interface that inherits it."><code>HTMLElement</code></a> object interface they also have available to them by inheritance) for manipulating the layout and presentation of <a href="/en-US/docs/Web/HTML/Element/meter" title="The HTML <meter> element represents either a scalar value within a known range or a fractional value."><code>&lt;meter&gt;</code></a> elements.</div>

## Properties

<ul class="items properties">
  <li>
    <a href="">high</a>
    <div>A <code>double</code> representing the value of the high boundary, reflecting the <code><a href="/en-US/docs/Web/HTML/Element/meter#attr-high">high</a></code> attribute.</div>
  </li>
  <li>
    <a href="">labels</a>
    <div>A <a href="/en-US/docs/Web/API/NodeList" title="NodeList objects are collections of nodes, usually returned by properties such as Node.childNodes and methods such as document.querySelectorAll()."><code>NodeList</code></a> of <a href="/en-US/docs/Web/HTML/Element/label" title="The HTML <label> element represents a caption for an item in a user interface."><code>&lt;label&gt;</code></a> elements that are associated with the element.</div>
  </li>
  <li>
    <a href="">low</a>
    <div>A <code>double</code> representing the value of the low boundary, reflecting the <code><a href="/en-US/docs/Web/HTML/Element/meter#attr-low">low</a></code>attribute.</div>
  </li>
  <li>
    <a href="">max</a>
    <div>A <code>double</code> representing the maximum value, reflecting the <code><a href="/en-US/docs/Web/HTML/Element/meter#attr-max">max</a></code> attribute.</div>
  </li>
  <li>
    <a href="">min</a>
    <div>A <code>double</code> representing the minimum value, reflecting the <code><a href="/en-US/docs/Web/HTML/Element/meter#attr-min">min</a></code> attribute.</div>
  </li>
  <li>
    <a href="">optimum</a>
    <div>A <code>double</code> representing the optimum, reflecting the <code><a href="/en-US/docs/Web/HTML/Element/meter#attr-optimum">optimum</a></code> attribute.</div>
  </li>
  <li>
    <a href="">value</a>
    <div>A <code>double</code> representing the currrent value, reflecting the <code><a href="/en-US/docs/Web/HTML/Element/meter#attr-value">value</a></code> attribute.</div>
  </li>
</ul>

## Methods

<ul class="items methods">

</ul>

## Events
