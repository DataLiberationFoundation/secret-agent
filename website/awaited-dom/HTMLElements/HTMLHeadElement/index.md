# HTMLHeadElement

<div class='overview'>The <strong><code>HTMLHeadElement</code></strong> interface contains the descriptive information, or metadata, for a document. This object inherits all of the properties and methods described in the <a href="/en-US/docs/Web/API/HTMLElement" title="The HTMLElement interface represents any HTML element. Some elements directly implement this interface, while others implement it via an interface that inherits it."><code>HTMLElement</code></a> interface.</div>

## Properties

<ul class="items properties">

</ul>

## Methods

<ul class="items methods">

</ul>

## Events
