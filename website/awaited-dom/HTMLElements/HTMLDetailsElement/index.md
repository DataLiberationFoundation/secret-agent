# HTMLDetailsElement

<div class='overview'>The <strong><code>HTMLDetailsElement</code></strong> interface provides special properties (beyond the regular <a href="/en-US/docs/Web/API/HTMLElement" title="The HTMLElement interface represents any HTML element. Some elements directly implement this interface, while others implement it via an interface that inherits it."><code>HTMLElement</code></a> interface it also has available to it by inheritance) for manipulating <a href="/en-US/docs/Web/HTML/Element/details" title="The HTML Details Element (<details>) creates a disclosure widget in which information is visible only when the widget is toggled into an &quot;open&quot; state."><code>&lt;details&gt;</code></a> elements.</div>

## Properties

<ul class="items properties">
  <li>
    <a href="">open</a>
    <div>Is a <a href="/en-US/docs/Web/API/Boolean" title="REDIRECT Boolean [en-US]"><code>boolean</code></a> reflecting the <code><a href="/en-US/docs/Web/HTML/Element/details#attr-open">open</a></code> HTML attribute, indicating whether or not the element’s contents (not counting the <a href="/en-US/docs/Web/HTML/Element/summary" title="The HTML Disclosure Summary element (<summary>) element specifies a summary, caption, or legend for a <details> element's disclosure box."><code>&lt;summary&gt;</code></a>) is to be shown to the user.</div>
  </li>
</ul>

## Methods

<ul class="items methods">

</ul>

## Events
