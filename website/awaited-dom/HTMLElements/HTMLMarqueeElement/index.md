# HTMLMarqueeElement

<div class='overview'><strong>Draft</strong><br>
    This page is not complete.</div>

## Properties

<ul class="items properties">
  <li>
    <a href="">behavior</a>
    <div>Sets how the text is scrolled within the marquee. Possible values are <code>scroll</code>, <code>slide</code> and <code>alternate</code>. If no value is specified, the default value is <code>scroll</code>.</div>
  </li>
  <li>
    <a href="">bgColor</a>
    <div>Sets the background color through color name or hexadecimal value.</div>
  </li>
  <li>
    <a href="">direction</a>
    <div>Sets the direction of the scrolling within the marquee. Possible values are <code>left</code>, <code>right</code>, <code>up</code> and <code>down</code>. If no value is specified, the default value is <code>left</code>.</div>
  </li>
  <li>
    <a href="">height</a>
    <div>Sets the height in pixels or percentage value.</div>
  </li>
  <li>
    <a href="">hspace</a>
    <div>Sets the horizontal margin.</div>
  </li>
  <li>
    <a href="">scrollAmount</a>
    <div>Sets the amount of scrolling at each interval in pixels. The default value is 6.</div>
  </li>
  <li>
    <a href="">scrollDelay</a>
    <div>Sets the interval between each scroll movement in milliseconds. The default value is 85. Note that any value smaller than 60 is ignored and the value 60 is used instead, unless <code>trueSpeed</code> is <code>true</code>.</div>
  </li>
  <li>
    <a href="">trueSpeed</a>
    <div>By default, <code>scrollDelay</code> values lower than 60 are ignored. If <code>trueSpeed</code> is <code>true</code>, then those values are not ignored.</div>
  </li>
  <li>
    <a href="">vspace</a>
    <div>Sets the vertical margin.</div>
  </li>
  <li>
    <a href="">width</a>
    <div>Sets the width in pixels or percentage value.</div>
  </li>
</ul>

## Methods

<ul class="items methods">

</ul>

## Events
