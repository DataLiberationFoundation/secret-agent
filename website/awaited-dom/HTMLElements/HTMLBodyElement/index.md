# HTMLBodyElement

<div class='overview'>The <strong><code>HTMLBodyElement</code></strong> interface provides special properties (beyond those inherited from the regular <a href="/en-US/docs/Web/API/HTMLElement" title="The HTMLElement interface represents any HTML element. Some elements directly implement this interface, while others implement it via an interface that inherits it."><code>HTMLElement</code></a> interface) for manipulating <a href="/en-US/docs/Web/HTML/Element/body" title="The HTML <body> Element represents the content of an HTML&nbsp;document. There can be only one <body> element in a document."><code>&lt;body&gt;</code></a> elements.</div>

## Properties

<ul class="items properties">
  <li>
    <a href="">aLink</a>
    <div>Is a <a href="/en-US/docs/Web/API/DOMString" title="DOMString is a UTF-16 String. As JavaScript already uses such strings, DOMString is mapped directly to a String."><code>DOMString</code></a> that represents the color of active hyperlinks.</div>
  </li>
  <li>
    <a href="">background</a>
    <div>Is a <a href="/en-US/docs/Web/API/DOMString" title="DOMString is a UTF-16 String. As JavaScript already uses such strings, DOMString is mapped directly to a String."><code>DOMString</code></a> that represents the description of the location of the background image resource. Note that this is not an URI, though some older version of some browsers do expect it.</div>
  </li>
  <li>
    <a href="">bgColor</a>
    <div>Is a <a href="/en-US/docs/Web/API/DOMString" title="DOMString is a UTF-16 String. As JavaScript already uses such strings, DOMString is mapped directly to a String."><code>DOMString</code></a> that represents the background color for the document.</div>
  </li>
  <li>
    <a href="">link</a>
    <div>Is a <a href="/en-US/docs/Web/API/DOMString" title="DOMString is a UTF-16 String. As JavaScript already uses such strings, DOMString is mapped directly to a String."><code>DOMString</code></a> that represents the color of unvisited links.</div>
  </li>
  <li>
    <a href="">text</a>
    <div>Is a <a href="/en-US/docs/Web/API/DOMString" title="DOMString is a UTF-16 String. As JavaScript already uses such strings, DOMString is mapped directly to a String."><code>DOMString</code></a> that represents the foreground color of text.</div>
  </li>
  <li>
    <a href="">vLink</a>
    <div>Is a <a href="/en-US/docs/Web/API/DOMString" title="DOMString is a UTF-16 String. As JavaScript already uses such strings, DOMString is mapped directly to a String."><code>DOMString</code></a> that represents the color of visited links.</div>
  </li>
</ul>

## Methods

<ul class="items methods">

</ul>

## Events
