# HTMLTableCaptionElement

<div class='overview'>The <strong><code>HTMLTableCaptionElement</code></strong> interface special properties (beyond the regular <a href="/en-US/docs/Web/API/HTMLElement" title="The HTMLElement interface represents any HTML element. Some elements directly implement this interface, while others implement it via an interface that inherits it."><code>HTMLElement</code></a> interface it also has available to it by inheritance) for manipulating table caption elements.</div>

## Properties

<ul class="items properties">
  <li>
    <a href="">align</a>
    <div>Is a <a href="/en-US/docs/Web/API/DOMString" title="DOMString is a UTF-16 String. As JavaScript already uses such strings, DOMString is mapped directly to a String."><code>DOMString</code></a> which represents an enumerated attribute indicating alignment of the caption with respect to the table.</div>
  </li>
</ul>

## Methods

<ul class="items methods">

</ul>

## Events
