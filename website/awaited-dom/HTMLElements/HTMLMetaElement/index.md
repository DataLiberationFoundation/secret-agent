# HTMLMetaElement

<div class='overview'>The <strong><code>HTMLMetaElement</code></strong> interface contains descriptive metadata about a document. It&nbsp;inherits all of the properties and methods described in the <span class="internal"><a href="/en-US/docs/Web/API/HTMLElement" title="The HTMLElement interface represents any HTML element. Some elements directly implement this interface, while others implement it via an interface that inherits it."><code>HTMLElement</code></a> interface</span>.</div>

## Properties

<ul class="items properties">
  <li>
    <a href="">content</a>
    <div>Gets or sets the value of meta-data property.</div>
  </li>
  <li>
    <a href="">httpEquiv</a>
    <div>Gets or sets the name of an HTTP&nbsp;response header to define for a document.</div>
  </li>
  <li>
    <a href="">name</a>
    <div>Gets or sets the name of a meta-data property to define for a document.</div>
  </li>
  <li>
    <a href="">scheme</a>
    <div>Gets or sets the name of a scheme used to interpret the value of a meta-data property.</div>
  </li>
</ul>

## Methods

<ul class="items methods">

</ul>

## Events
