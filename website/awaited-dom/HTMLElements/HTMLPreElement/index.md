# HTMLPreElement

<div class='overview'>The <strong><code>HTMLPreElement</code></strong> interface exposes specific properties and methods (beyond those of the <a href="/en-US/docs/Web/API/HTMLElement" title="The HTMLElement interface represents any HTML element. Some elements directly implement this interface, while others implement it via an interface that inherits it."><code>HTMLElement</code></a> interface it also has available to it by inheritance) for manipulating a block of preformatted text (<a href="/en-US/docs/Web/HTML/Element/pre" title="The HTML <pre> element represents preformatted text which is to be presented exactly as written in the HTML file."><code>&lt;pre&gt;</code></a>).</div>

## Properties

<ul class="items properties">
  <li>
    <a href="">width</a>
    <div>Is a <code>long</code> value reflecting the obsolete <code><a href="/en-US/docs/Web/HTML/Element/pre#attr-width">width</a></code> attribute, containing a fixed-size length for the <a href="/en-US/docs/Web/HTML/Element/pre" title="The HTML <pre> element represents preformatted text which is to be presented exactly as written in the HTML file."><code>&lt;pre&gt;</code></a> element.</div>
  </li>
</ul>

## Methods

<ul class="items methods">

</ul>

## Events
