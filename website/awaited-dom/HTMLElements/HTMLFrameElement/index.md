# HTMLFrameElement

## Properties

<ul class="items properties">
  <li>
    <a href="">contentDocument</a>
    <div></div>
  </li>
  <li>
    <a href="">contentWindow</a>
    <div></div>
  </li>
  <li>
    <a href="">frameBorder</a>
    <div></div>
  </li>
  <li>
    <a href="">longDesc</a>
    <div></div>
  </li>
  <li>
    <a href="">marginHeight</a>
    <div></div>
  </li>
  <li>
    <a href="">marginWidth</a>
    <div></div>
  </li>
  <li>
    <a href="">name</a>
    <div></div>
  </li>
  <li>
    <a href="">noResize</a>
    <div></div>
  </li>
  <li>
    <a href="">scrolling</a>
    <div></div>
  </li>
  <li>
    <a href="">src</a>
    <div></div>
  </li>
</ul>

## Methods

<ul class="items methods">

</ul>

## Events
