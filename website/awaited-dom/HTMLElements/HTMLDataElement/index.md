# HTMLDataElement

<div class='overview'>The <strong><code>HTMLDataElement</code></strong> interface provides special properties (beyond the regular <a href="/en-US/docs/Web/API/HTMLElement" title="The HTMLElement interface represents any HTML element. Some elements directly implement this interface, while others implement it via an interface that inherits it."><code>HTMLElement</code></a> interface it also has available to it by inheritance) for manipulating <a href="/en-US/docs/Web/HTML/Element/data" title="The HTML <data> element links a given content with a machine-readable translation. If the content is time- or date-related, the <time> element must be used."><code>&lt;data&gt;</code></a> elements.</div>

## Properties

<ul class="items properties">
  <li>
    <a href="">value</a>
    <div>Is a <a href="/en-US/docs/Web/API/DOMString" title="DOMString is a UTF-16 String. As JavaScript already uses such strings, DOMString is mapped directly to a String."><code>DOMString</code></a> reflecting the <code><a href="/en-US/docs/Web/HTML/Element/data#attr-value">value</a></code> HTML attribute, containing a machine-readable form of the element's value.</div>
  </li>
</ul>

## Methods

<ul class="items methods">

</ul>

## Events
