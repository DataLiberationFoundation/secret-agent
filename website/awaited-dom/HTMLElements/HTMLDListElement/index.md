# HTMLDListElement

<div class='overview'>The <strong><code>HTMLDListElement</code></strong> interface provides special properties (beyond those of the regular <a href="/en-US/docs/Web/API/HTMLElement" title="The HTMLElement interface represents any HTML element. Some elements directly implement this interface, while others implement it via an interface that inherits it."><code>HTMLElement</code></a> interface it also has available to it by inheritance) for manipulating definition list (<a href="/en-US/docs/Web/HTML/Element/dl" title="The HTML <dl> element represents a description list. The element encloses a list of groups of terms (specified using the <dt> element) and descriptions (provided by <dd> elements). Common uses for this element are to implement a glossary or to display metadata (a list of key-value pairs)."><code>&lt;dl&gt;</code></a>) elements.</div>

## Properties

<ul class="items properties">
  <li>
    <a href="">compact</a>
    <div>Is a <a href="/en-US/docs/Web/API/Boolean" title="REDIRECT Boolean [en-US]"><code>Boolean</code></a> indicating that spacing between list items should be reduced.</div>
  </li>
</ul>

## Methods

<ul class="items methods">

</ul>

## Events
