# HTMLFrameSetElement

<div class='overview'><strong>Obsolete</strong><br>This feature is obsolete. Although it may still work in some browsers, its use is discouraged since it could be removed at any time. Try to avoid using it.</div>

## Properties

<ul class="items properties">
  <li>
    <a href="">cols</a>
    <div>Is a <a href="/en-US/docs/Web/API/DOMString" title="DOMString is a UTF-16 String. As JavaScript already uses such strings, DOMString is mapped directly to a String."><code>DOMString</code></a> structured as a comma-seperated list&nbsp;specifing&nbsp;the width of each column inside&nbsp;a frameset.</div>
  </li>
  <li>
    <a href="">rows</a>
    <div>Is a <a href="/en-US/docs/Web/API/DOMString" title="DOMString is a UTF-16 String. As JavaScript already uses such strings, DOMString is mapped directly to a String."><code>DOMString</code></a> structured as a comma-seperated list&nbsp;specifing&nbsp;the height of each column inside&nbsp;a frameset.</div>
  </li>
</ul>

## Methods

<ul class="items methods">

</ul>

## Events
