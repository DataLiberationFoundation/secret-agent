# HTMLHRElement

<div class='overview'>The <strong><code>HTMLHRElement</code></strong> interface provides special properties (beyond those of the <a href="/en-US/docs/Web/API/HTMLElement" title="The HTMLElement interface represents any HTML element. Some elements directly implement this interface, while others implement it via an interface that inherits it."><code>HTMLElement</code></a> interface it also has available to it by inheritance) for manipulating <a href="/en-US/docs/Web/HTML/Element/hr" title="The HTML <hr> element represents a thematic break between paragraph-level elements: for example, a change of scene in a story, or a shift of topic within a section."><code>&lt;hr&gt;</code></a> elements.</div>

## Properties

<ul class="items properties">

</ul>

## Methods

<ul class="items methods">

</ul>

## Events
