# SVGAnimatedLength

<div class='overview'>The <code>SVGAnimatedLength</code> interface is used for attributes of basic type <a href="/en/SVG/Content_type#Length" title="https://developer.mozilla.org/en/SVG/Content_type#Length">&lt;length&gt;</a> which can be animated.</div>

## Properties

<ul class="items properties">
  <li>
    <a href="">animVal</a>
    <div>If the given attribute or property is being animated, contains the current animated value of the attribute or property. If the given attribute or property is not currently being animated, contains the same value as <code>baseVal</code>.</div>
  </li>
  <li>
    <a href="">baseVal</a>
    <div>The base value of the given attribute before applying any animations.</div>
  </li>
</ul>

## Methods

<ul class="items methods">

</ul>

## Events
