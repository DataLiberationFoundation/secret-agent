# SVGElement

<div class='overview'>All of the SVG DOM interfaces that correspond directly to elements in the SVG language derive from the <code>SVGElement</code> interface.</div>

## Properties

<ul class="items properties">

</ul>

## Methods

<ul class="items methods">

</ul>

## Events
