# SVGTitleElement

<div class='overview'>The <strong><code>SVGTitleElement</code></strong> interface corresponds to the <a href="/en-US/docs/Web/SVG/Element/title" title="The <title> element provides an accessible, short-text description of any SVG container element or graphics element."><code>&lt;title&gt;</code></a> element.</div>

## Properties

<ul class="items properties">

</ul>

## Methods

<ul class="items methods">

</ul>

## Events
