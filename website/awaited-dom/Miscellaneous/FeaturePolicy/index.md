# FeaturePolicy

## Properties

<ul class="items properties">

</ul>

## Methods

<ul class="items methods">
  <li>
    <a href="">allowedFeatures()</a>
    <div></div>
  </li>
  <li>
    <a href="">allowsFeature()</a>
    <div></div>
  </li>
  <li>
    <a href="">features()</a>
    <div></div>
  </li>
  <li>
    <a href="">getAllowlistForFeature()</a>
    <div></div>
  </li>
</ul>

## Events
