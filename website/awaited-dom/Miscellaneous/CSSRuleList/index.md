# CSSRuleList

<div class='overview'>A <code>CSSRuleList</code> is an (indirect-modify only) array-like object containing an ordered collection of <code><a href="/en/DOM/cssRule" title="en/DOM/cssRule">CSSRule</a></code> objects.</div>

## Properties

<ul class="items properties">
  <li>
    <a href="">length</a>
    <div>Returns the number of items in the collection.</div>
  </li>
</ul>

## Methods

<ul class="items methods">
  <li>
    <a href="">item()</a>
    <div>Returns the specific node at the given zero-based index into the list. Returns null if the index is out of range.</div>
  </li>
</ul>

## Events
