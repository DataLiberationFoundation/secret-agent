# Animation

## Properties

<ul class="items properties">
  <li>
    <a href="">currentTime</a>
    <div></div>
  </li>
  <li>
    <a href="">effect</a>
    <div></div>
  </li>
  <li>
    <a href="">finished</a>
    <div></div>
  </li>
  <li>
    <a href="">id</a>
    <div></div>
  </li>
  <li>
    <a href="">oncancel</a>
    <div></div>
  </li>
  <li>
    <a href="">onfinish</a>
    <div></div>
  </li>
  <li>
    <a href="">onremove</a>
    <div></div>
  </li>
  <li>
    <a href="">pending</a>
    <div></div>
  </li>
  <li>
    <a href="">playbackRate</a>
    <div></div>
  </li>
  <li>
    <a href="">playState</a>
    <div></div>
  </li>
  <li>
    <a href="">ready</a>
    <div></div>
  </li>
  <li>
    <a href="">replaceState</a>
    <div></div>
  </li>
  <li>
    <a href="">startTime</a>
    <div></div>
  </li>
  <li>
    <a href="">timeline</a>
    <div></div>
  </li>
</ul>

## Methods

<ul class="items methods">
  <li>
    <a href="">cancel()</a>
    <div></div>
  </li>
  <li>
    <a href="">commitStyles()</a>
    <div></div>
  </li>
  <li>
    <a href="">finish()</a>
    <div></div>
  </li>
  <li>
    <a href="">pause()</a>
    <div></div>
  </li>
  <li>
    <a href="">persist()</a>
    <div></div>
  </li>
  <li>
    <a href="">play()</a>
    <div></div>
  </li>
  <li>
    <a href="">reverse()</a>
    <div></div>
  </li>
  <li>
    <a href="">updatePlaybackRate()</a>
    <div></div>
  </li>
</ul>

## Events
