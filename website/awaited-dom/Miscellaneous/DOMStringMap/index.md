# DOMStringMap

<div class='overview'><span class="seoSummary">Used by the </span><a href="/en-US/docs/Web/API/HTMLElement/dataset" title="The documentation about this has not yet been written; please consider contributing!"><code>HTMLElement.dataset</code></a><span class="seoSummary"> attribute to represent data for custom attributes added to elements.</span></div>

## Properties

<ul class="items properties">

</ul>

## Methods

<ul class="items methods">

</ul>

## Events
