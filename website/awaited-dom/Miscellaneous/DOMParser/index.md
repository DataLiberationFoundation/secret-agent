# DOMParser

<div class='overview'><span class="seoSummary">The <strong><code>DOMParser</code></strong> interface provides the ability to parse <a class="glossaryLink" href="/en-US/docs/Glossary/XML" title="XML: eXtensible Markup Language (XML) is a generic markup language specified by the W3C. The information technology (IT) industry uses many languages based on XML as data-description languages.">XML</a> or <a class="glossaryLink" href="/en-US/docs/Glossary/HTML" title="HTML: HTML (HyperText Markup Language) is a descriptive language that specifies webpage structure.">HTML</a> source code from a string into a DOM <a href="/en-US/docs/Web/API/Document" title="The Document interface represents any web page loaded in the browser and serves as an entry point into the web page's content, which is the DOM tree."><code>Document</code></a>.</span></div>

## Properties

<ul class="items properties">

</ul>

## Methods

<ul class="items methods">
  <li>
    <a href="">parseFromString()</a>
    <div>Parse XML from a string</div>
  </li>
</ul>

## Events
