# XPathResult

## Properties

<ul class="items properties">
  <li>
    <a href="">booleanValue</a>
    <div></div>
  </li>
  <li>
    <a href="">invalidIteratorState</a>
    <div></div>
  </li>
  <li>
    <a href="">numberValue</a>
    <div></div>
  </li>
  <li>
    <a href="">resultType</a>
    <div></div>
  </li>
  <li>
    <a href="">singleNodeValue</a>
    <div></div>
  </li>
  <li>
    <a href="">snapshotLength</a>
    <div></div>
  </li>
  <li>
    <a href="">stringValue</a>
    <div></div>
  </li>
</ul>

## Methods

<ul class="items methods">
  <li>
    <a href="">iterateNext()</a>
    <div></div>
  </li>
  <li>
    <a href="">snapshotItem()</a>
    <div></div>
  </li>
</ul>

## Events
