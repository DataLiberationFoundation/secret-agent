# StylePropertyMapReadOnly

## Properties

<ul class="items properties">
  <li>
    <a href="">size</a>
    <div></div>
  </li>
</ul>

## Methods

<ul class="items methods">
  <li>
    <a href="">entries()</a>
    <div></div>
  </li>
  <li>
    <a href="">forEach()</a>
    <div></div>
  </li>
  <li>
    <a href="">get()</a>
    <div></div>
  </li>
  <li>
    <a href="">getAll()</a>
    <div></div>
  </li>
  <li>
    <a href="">has()</a>
    <div></div>
  </li>
  <li>
    <a href="">keys()</a>
    <div></div>
  </li>
  <li>
    <a href="">values()</a>
    <div></div>
  </li>
</ul>

## Events
