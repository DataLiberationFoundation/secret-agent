# HTMLOrSVGElement

## Properties

<ul class="items properties">
  <li>
    <a href="">dataset</a>
    <div></div>
  </li>
  <li>
    <a href="">nonce</a>
    <div></div>
  </li>
  <li>
    <a href="">tabIndex</a>
    <div></div>
  </li>
</ul>

## Methods

<ul class="items methods">
  <li>
    <a href="">blur()</a>
    <div></div>
  </li>
  <li>
    <a href="">focus()</a>
    <div></div>
  </li>
</ul>

## Events
