# DOMRect

<div class='overview'><strong>Draft</strong><br>
    This page is not complete.</div>

## Properties

<ul class="items properties">
  <li>
    <a href="">height</a>
    <div>The height of the <code>DOMRect</code>.</div>
  </li>
  <li>
    <a href="">width</a>
    <div>The width of the <code>DOMRect</code>.</div>
  </li>
  <li>
    <a href="">x</a>
    <div>The x coordinate of the <code>DOMRect</code>'s origin.</div>
  </li>
  <li>
    <a href="">y</a>
    <div>The y coordinate of the <code>DOMRect</code>'s origin.</div>
  </li>
</ul>

## Methods

<ul class="items methods">
  <li>
    <a href="">fromRect()</a>
    <div>Creates a new <code>DOMRect</code>&nbsp;object with a given location and dimensions.</div>
  </li>
</ul>

## Events
