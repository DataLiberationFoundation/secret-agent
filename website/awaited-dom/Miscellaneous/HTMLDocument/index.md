# HTMLDocument

<div class='overview'><span class="seoSummary">The <strong><code>HTMLDocument</code></strong> interface, which may be accessed through the <code>Window.HTMLDocument</code> property, extends the <code>Window.HTMLDocument</code> property to include methods and properties that are specific to <a class="glossaryLink" href="/en-US/docs/Glossary/HTML" title="HTML: HTML (HyperText Markup Language) is a descriptive language that specifies webpage structure.">HTML</a> documents.</span></div>

## Properties

<ul class="items properties">

</ul>

## Methods

<ul class="items methods">

</ul>

## Events
