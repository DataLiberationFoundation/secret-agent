# AnimationEffect

## Properties

<ul class="items properties">

</ul>

## Methods

<ul class="items methods">
  <li>
    <a href="">getComputedTiming()</a>
    <div></div>
  </li>
  <li>
    <a href="">getTiming()</a>
    <div></div>
  </li>
  <li>
    <a href="">updateTiming()</a>
    <div></div>
  </li>
</ul>

## Events
