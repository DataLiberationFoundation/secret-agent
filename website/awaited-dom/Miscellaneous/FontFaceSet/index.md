# FontFaceSet

## Properties

<ul class="items properties">
  <li>
    <a href="">onloading</a>
    <div></div>
  </li>
  <li>
    <a href="">onloadingdone</a>
    <div></div>
  </li>
  <li>
    <a href="">onloadingerror</a>
    <div></div>
  </li>
  <li>
    <a href="">ready</a>
    <div></div>
  </li>
  <li>
    <a href="">status</a>
    <div></div>
  </li>
</ul>

## Methods

<ul class="items methods">
  <li>
    <a href="">add()</a>
    <div></div>
  </li>
  <li>
    <a href="">check()</a>
    <div></div>
  </li>
  <li>
    <a href="">clear()</a>
    <div></div>
  </li>
  <li>
    <a href="">delete()</a>
    <div></div>
  </li>
  <li>
    <a href="">forEach()</a>
    <div></div>
  </li>
  <li>
    <a href="">load()</a>
    <div></div>
  </li>
</ul>

## Events
