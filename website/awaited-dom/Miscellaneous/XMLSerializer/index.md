# XMLSerializer

<div class='overview'><span class="seoSummary">The <code>XMLSerializer</code> interface provides the <a href="/en-US/docs/Web/API/XMLSerializer/serializeToString" title="The XMLSerializer method serializeToString() constructs a string representing the specified DOM tree in XML form."><code>serializeToString()</code></a> method to construct an XML string representing a <a class="glossaryLink" href="/en-US/docs/Glossary/DOM" title="DOM: The DOM (Document Object Model) is an API that represents and interacts with any HTML or XML document. The DOM is a document model loaded in the browser and representing the document as a node tree, where each node represents part of the document (e.g. an element, text string, or comment).">DOM</a> tree.</span></div>

## Properties

<ul class="items properties">

</ul>

## Methods

<ul class="items methods">
  <li>
    <a href="">serializeToString()</a>
    <div>Returns the serialized subtree of a string.</div>
  </li>
</ul>

## Events
