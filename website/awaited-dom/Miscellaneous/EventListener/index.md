# EventListener

<div class='overview'>The <strong><code>EventListener</code></strong> interface represents an object that can handle an event dispatched by an <a href="/en-US/docs/Web/API/EventTarget" title="EventTarget is a DOM interface implemented by objects that can receive events and may have listeners for them."><code>EventTarget</code></a> object.</div>

## Properties

<ul class="items properties">

</ul>

## Methods

<ul class="items methods">
  <li>
    <a href="">handleEvent()</a>
    <div>A function that is called whenever an event of the specified type occurs.</div>
  </li>
</ul>

## Events
