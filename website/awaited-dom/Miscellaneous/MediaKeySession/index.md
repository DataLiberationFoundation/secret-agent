# MediaKeySession

## Properties

<ul class="items properties">
  <li>
    <a href="">closed</a>
    <div></div>
  </li>
  <li>
    <a href="">expiration</a>
    <div></div>
  </li>
  <li>
    <a href="">keyStatuses</a>
    <div></div>
  </li>
  <li>
    <a href="">onkeystatuseschange</a>
    <div></div>
  </li>
  <li>
    <a href="">onmessage</a>
    <div></div>
  </li>
  <li>
    <a href="">sessionId</a>
    <div></div>
  </li>
</ul>

## Methods

<ul class="items methods">
  <li>
    <a href="">close()</a>
    <div></div>
  </li>
  <li>
    <a href="">generateRequest()</a>
    <div></div>
  </li>
  <li>
    <a href="">load()</a>
    <div></div>
  </li>
  <li>
    <a href="">remove()</a>
    <div></div>
  </li>
  <li>
    <a href="">update()</a>
    <div></div>
  </li>
</ul>

## Events
