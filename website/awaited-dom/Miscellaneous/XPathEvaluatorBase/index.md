# XPathEvaluatorBase

## Properties

<ul class="items properties">

</ul>

## Methods

<ul class="items methods">
  <li>
    <a href="">createExpression()</a>
    <div></div>
  </li>
  <li>
    <a href="">createNSResolver()</a>
    <div></div>
  </li>
  <li>
    <a href="">evaluate()</a>
    <div></div>
  </li>
</ul>

## Events
