# DOMRectReadOnly

<div class='overview'>The <strong><code>DOMRectReadOnly</code></strong> interface specifies the standard properties used by <a href="/en-US/docs/Web/API/DOMRect" title="A DOMRect represents a rectangle."><code>DOMRect</code></a> to define a rectangle whose properties are immutable.</div>

## Properties

<ul class="items properties">
  <li>
    <a href="">bottom</a>
    <div>Returns the bottom coordinate value of the <code>DOMRect</code> (usually the same as y + height).</div>
  </li>
  <li>
    <a href="">height</a>
    <div>The height of the <code>DOMRect</code>.</div>
  </li>
  <li>
    <a href="">left</a>
    <div>Returns the left coordinate value of the <code>DOMRect</code> (usually the same as <code>x</code>).</div>
  </li>
  <li>
    <a href="">right</a>
    <div>Returns the right coordinate value of the <code>DOMRect</code> (usually the same as <code>x + width</code>).</div>
  </li>
  <li>
    <a href="">top</a>
    <div>Returns the top coordinate value of the <code>DOMRect</code> (usually the same as <code>y</code>.)</div>
  </li>
  <li>
    <a href="">width</a>
    <div>The width of the <code>DOMRect</code>.</div>
  </li>
  <li>
    <a href="">x</a>
    <div>The x coordinate of the <code>DOMRect</code>'s origin.</div>
  </li>
  <li>
    <a href="">y</a>
    <div>The y coordinate of the <code>DOMRect</code>'s origin.</div>
  </li>
</ul>

## Methods

<ul class="items methods">
  <li>
    <a href="">fromRect()</a>
    <div>Creates a new <code>DOMRect</code>&nbsp;object with a given location and dimensions.</div>
  </li>
</ul>

## Events
