# SVGTests

<div class='overview'>The <strong><code>SVGTests</code></strong> interface is used to reflect conditional processing attributes and is mixed into other interfaces for elements that support these attributes.</div>

## Properties

<ul class="items properties">
  <li>
    <a href="">requiredExtensions</a>
    <div>An <a href="/en-US/docs/Web/API/SVGStringList" title="The SVGStringList defines a list of DOMString objects."><code>SVGStringList</code></a> corresponding to the <code><a class="new" href="/en-US/docs/Web/SVG/Attribute/requiredExtensions" rel="nofollow">requiredExtensions</a></code> attribute of the given element.</div>
  </li>
  <li>
    <a href="">systemLanguage</a>
    <div>An <a href="/en-US/docs/Web/API/SVGStringList" title="The SVGStringList defines a list of DOMString objects."><code>SVGStringList</code></a> corresponding to the <code><a href="/en-US/docs/Web/SVG/Attribute/systemLanguage">systemLanguage</a></code> attribute of the given element.</div>
  </li>
</ul>

## Methods

<ul class="items methods">

</ul>

## Events
