# The Awaited DOM

> SecretAgent's Awaited DOM is an implementation of the W3C DOM specification for NodeJS.

Unlike Cheerio, JSDOM, and other parsing libraries, Noderdom strives to perfectly match the latest standards.


## Attributions

Most of the documentation contained on these pages ultimately came from the W3C specifications. A huge thanks to the Mozilla organization and their many volunteers for compiling this data into a single location on MDN.
