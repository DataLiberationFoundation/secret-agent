# AwaitedDOM
> The AwaitedDOM is a NodeJs implementation of W3C's DOM specification that makes it easy to call properties and methods located in a remote browser engine as if they were local to your scraper script context.

## What Properties and Methods Can I Use?

<div>
  <input type="text" placeholder="Search any DOM property, method, or interface..." />
</div>

## Introducing Supers

|   |   |
|---|---|
| [SuperDocument](/docs/awaited-dom/supers/super-document) | [SuperNode](/docs/awaited-dom/supers/super-node) |
| [SuperElement](/docs/awaited-dom/dynamic/document-ish) | [SuperNodeList](/docs/awaited-dom/dynamic/xml-document)  |
| [SuperHTMLCollection](/docs/awaited-dom/dynamic/document-ish) | [SuperText](/docs/awaited-dom/dynamic/xml-document)  |
| [SuperCharacterData](/docs/awaited-dom/dynamic/document-ish) | [SuperHTMLElement](/docs/awaited-dom/dynamic/xml-document)  |
| [SuperStyleSheet](/docs/awaited-dom/dynamic/document-ish) |   |


## Document Interfaces

|   |   |
|---|---|
| [SuperDocument](/docs/awaited-dom/dynamic/document-ish) | [HTMLDocument](/docs/awaited-dom/dynamic/html-document "The HTMLDocument interface includes properties and methods that are specific to HTML documents.") |
| [XMLDocument](/docs/awaited-dom/dynamic/document-ish "The XMLDocument interface includes properties and methods that are specific to XML documents.") | [DocumentFragment](/docs/awaited-dom/dynamic/xml-document "The DocumentFragment interface represents a minimal document object that has no parent.")  |

## Node Interfaces

|   |   |
|---|---|
| [SuperNode](./docs/awaited-dom/dynamic/document-ish) | [Node](/docs/awaited-dom/dynamic/html-document) |
| [Element](../../awaited-dom/dynamic/document-ish) | [Comment](/docs/awaited-dom/dynamic/xml-document) |
| [Text](/docs/awaited-dom/dynamic/document-ish) |  |

## HTML Elements

#### HTMLElement

## SVG Elements

#### SVGElement
